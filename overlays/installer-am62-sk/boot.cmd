env set tiboot3_file /deb-binaries/usr/lib/k3-image-gen/tiboot3-am62x-evm.bin
env set tiboot3_offset 0x0
env set tiboot3_size 0x400
env set tispl_file /deb-binaries/usr/lib/u-boot/am62x_evm_a53/tispl.bin
env set tispl_offset 0x400
env set tispl_size 0x1000
env set uboot_file /deb-binaries/usr/lib/u-boot/am62x_evm_a53/u-boot.img
env set uboot_offset 0x1400
env set uboot_size 0x2000
env set file_addr_r ${kernel_addr_r}
env set mmc_devs 0 1 2
env set sd_part 1

env set cmd_exit_err '
    echo "+-----------------------------------------------------------------+";
    echo "|                   U-Boot installation FAILED                    |";
    echo "|                                                                 |";
    echo "| Please check the SD Card and power cycle the board to continue  |";
    echo "+-----------------------------------------------------------------+";
    loop 0 0'

env set cmd_exit_ok '
    echo "+-------------------------------------------------------------------+";
    echo "|                  U-Boot installation complete                     |";
    echo "|                                                                   |";
    echo "| Please remove the SD Card, set the boot jumpers to boot from eMMC |";
    echo "|               and power cycle the board to continue               |";
    echo "+-------------------------------------------------------------------+";
    loop 0 0'

for d in ${mmc_devs}; do
    if test -z "${emmc_dev}" && mmc partconf ${d}; then
        env set emmc_dev ${d}
        echo "Found eMMC device: MMC ${emmc_dev}"
    fi
done

if test -z "${emmc_dev}"; then
    echo "Failed to find eMMC device"
    run cmd_exit_err
fi

# tiboot3 image
for d in ${mmc_devs}; do
    if test -z "${sd_dev}" && test -e mmc ${d}:${sd_part} ${tiboot3_file}; then
        env set sd_dev ${d}
        echo "Found tiboot3 image in: MMC ${sd_dev}:${sd_part}"
    fi
done

if test -z "${sd_dev}"; then
    echo "Failed to find tiboot3 image"
    run cmd_exit_err
fi

echo "Copying tiboot3 image from MMC ${sd_dev}:${sd_part} to RAM ${file_addr_r}"
load mmc ${sd_dev}:${sd_part} ${file_addr_r} ${tiboot3_file}
if test $? -ne 0; then
    echo "Failed to copy tiboot3 image to RAM"
    run cmd_exit_err
fi

echo "Writing tiboot3 to eMMC boot partition"
mmc dev ${emmc_dev} 1
mmc write ${file_addr_r} ${tiboot3_offset} ${tiboot3_size}
if test $? -ne 0; then
    echo "Failed to write tispl on eMMC boot partition"
    run cmd_exit_err
fi

# tispl image
if test -e mmc ${sd_dev}:${sd_part} ${tispl_file}; then
    echo "Found tispl image in: MMC ${sd_dev}:${sd_part}"
else
    echo "Failed to find tispl image"
    run cmd_exit_err
fi

echo "Copying tispl image from MMC ${sd_dev}:${sd_part} to RAM ${file_addr_r}"
load mmc ${sd_dev}:${sd_part} ${file_addr_r} ${tispl_file}
if test $? -ne 0; then
    echo "Failed to copy u-boot image to RAM"
    run cmd_exit_err
fi

echo "Writing tispl to eMMC boot partition"
mmc write ${file_addr_r} ${tispl_offset} ${tispl_size}
if test $? -ne 0; then
    echo "Failed to write tispl on eMMC boot partition"
    run cmd_exit_err
fi

# U-Boot image
if test -e mmc ${sd_dev}:${sd_part} ${uboot_file}; then
    echo "Found u-boot image in: MMC ${sd_dev}:${sd_part}"
else
    echo "Failed to find u-boot image"
    run cmd_exit_err
fi

echo "Copying u-boot image from MMC ${sd_dev}:${sd_part} to RAM ${file_addr_r}"
load mmc ${sd_dev}:${sd_part} ${file_addr_r} ${uboot_file}
if test $? -ne 0; then
    echo "Failed to copy u-boot image to RAM"
    run cmd_exit_err
fi

echo "Writing u-boot to eMMC boot partition"
mmc write ${file_addr_r} ${uboot_offset} ${uboot_size}
if test $? -ne 0; then
  echo "Failed to write u-boot on eMMC boot partition"
  run cmd_exit_err
fi

echo "Configuring eMMC boot partition"
# Set eMMC boot bus configuration
mmc bootbus ${emmc_dev} 2 0 0
# Enable eMMC boot partition 1 and boot ACK
mmc partconf ${emmc_dev} 1 1 1
run cmd_exit_ok
