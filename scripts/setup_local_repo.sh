#!/bin/sh

if [ "$#" -ne 4 ] ; then
    echo Usage setup_local_repo.sh REPO_FOLDER ROOTDIR CHROOT_REPO_FOLDER RELEASE
    exit 1
fi

REPO_FOLDER=$1
ROOTDIR=$2
CHROOT_REPO_FOLDER=$3
RELEASE=$4

SRCLIST=/etc/apt/sources.list

mount -o bind $REPO_FOLDER $ROOTDIR/$CHROOT_REPO_FOLDER

echo "deb [trusted=yes] file://$CHROOT_REPO_FOLDER $RELEASE target" >> $ROOTDIR/$SRCLIST