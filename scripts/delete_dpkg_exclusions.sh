#!/bin/sh

set -e

ALL=0
ROOTDIR=

usage() {
    echo "Usage: $0 [OPTIONS] ROOTDIR"
    echo "    ROOTDIR     Root directory"
    echo "    -a          All files (for fixed function images)"
}

while getopts "a" opt; do
    case $opt in
    h)
        usage
        ;;
    a)
        ALL=1
        ;;
    \?)
        echo Error: Unknown option >&2
        echo >&2
        usage
        exit 1
        ;;
    esac
done
shift $((OPTIND -1))

ROOTDIR=$1

if [ -z "${ROOTDIR}" ]; then
    usage
    exit 1
fi

# Delete doc files according to dpkg exclusions
find ${ROOTDIR}/usr/share/doc/ -depth -type f ! -name 'copyright_report*' ! -name '*.json*' -delete
find ${ROOTDIR}/usr/share/doc/ -depth -type d -empty -delete
rm -rf ${ROOTDIR}/usr/share/man/*
rm -rf ${ROOTDIR}/usr/share/info/*
rm -rf ${ROOTDIR}/usr/share/lintian/*
rm -rf ${ROOTDIR}/usr/share/linda/*
rm -rf ${ROOTDIR}/var/cache/man/*

# Delete remaining (all) files according to dpkg exclusions for fixed function images
if [ "${ALL}" = "1" ]; then
    rm -rf ${ROOTDIR}/usr/share/locale/*
    rm -rf ${ROOTDIR}/usr/share/i18n/*
    rm -rf ${ROOTDIR}/usr/share/zoneinfo/*

    rm -rf ${ROOTDIR}/lib/udev/hwdb.bin
    rm -rf ${ROOTDIR}/lib/udev/hwdb.d/*
fi