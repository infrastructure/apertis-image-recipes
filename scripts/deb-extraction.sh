#! /bin/bash
set -eu

ARCH=amd64
KEYRING=""
MIRROR=https://repositories.apertis.org/apertis/
OSNAME=apertis
PACKAGE=dash
SUITE=v2024

usage() {
    echo "$0 [OPTIONS]
    Extracts deb files

    options:
     -a architecture    Architecture
     -k keyring         Keyring file
     -m mirror          Mirror URL
     -p package         Package name
     -s suite           Suite
    "
    exit 1
}

while getopts ":a:hk:m:o:p:s:" opt; do
    case $opt in
    a)
        ARCH=$OPTARG
        ;;
    h)
        usage
        ;;
    k)
        KEYRING=$OPTARG
        ;;
    m)
        MIRROR=$OPTARG
        ;;
    o)
        OSNAME=$OPTARG
        ;;
    p)
        PACKAGE=$OPTARG
        ;;
    s)
        SUITE=$OPTARG
        ;;
    \?)
        echo Error: Unknown option: -$OPTARG >&2
        echo >&2
        usage
        exit 1
        ;;
    esac
done
shift $((OPTIND -1))

echo KEYRING $KEYRING

mkdir "./chdist"

echo "Setup chdist"
chdist -d "./chdist" create apertis-${SUITE} ${MIRROR} ${SUITE} target non-free

if [ -n "${KEYRING}" ]
then
    echo "Add Apertis gpg key to chdist"
    cp ${KEYRING} "./chdist/apertis-${SUITE}/etc/apt/trusted.gpg.d"
fi

echo "Update repo (chdist)"
chdist -d "./chdist" -a ${ARCH} apt apertis-${SUITE} update

echo "Download Firmare ${PACKAGE} (chdist)"
chdist -d "./chdist" -a ${ARCH} apt apertis-${SUITE} download ${PACKAGE}
echo "Unpack firmware ${PACKAGE}"
dpkg -X ${PACKAGE}*.deb "./deb-binaries"

rm -r "./chdist"
