#!/bin/sh

set -eu

SRCLIST=/etc/apt/sources.list
AUTHLIST=/etc/apt/auth.conf
SOURCES=
MIRROR=
RELEASE=
SECURITY=
UPDATES=
SNAPSHOT=
AUTHREGEX="^(.+?\/\/)(.+?):(.+?)@(.+)$"

opts=$(getopt -o "m:r:" -l "sources,mirror:,release:,updates,security,snapshot:" -- "$@")
eval set -- "$opts"

while [ $# -gt 0 ]; do
    case $1 in
        --sources) SOURCES=1; shift;;
        -m|--mirror) MIRROR="$2"; shift 2;;
        -r|--release) RELEASE="$2"; shift 2;;
        --updates) UPDATES=1; shift;;
        --security) SECURITY=1; shift;;
        --snapshot) SNAPSHOT="$2"; shift 2;;
        --) shift; break;;
        *) ;;
    esac
done

if [ -z "$MIRROR" ] || [ -z "$RELEASE" ] || [ $# -eq 0 ]; then
    echo "Please provide mirror, release and distribution(s)." >&2
    exit 1
fi

add_entry () {
    local MIRROR=$1
    local RELEASE=$2
    local dist=$3
    local SRCLIST=$4
    local SOURCES=$5

    [ -n "$SNAPSHOT" ] && RELEASE=$RELEASE/snapshots/$SNAPSHOT

    if ! grep -q "^deb .*$MIRROR.*$RELEASE.*$dist" $SRCLIST; then
        echo "deb $MIRROR $RELEASE $dist" >> $SRCLIST
    fi

    if test -n "$SOURCES" && ! grep -q "^deb-src .*$MIRROR.*$RELEASE.*$dist" $SRCLIST; then
        echo "deb-src $MIRROR $RELEASE $dist" >> $SRCLIST
    fi
}

for dist in $@; do
    add_entry $MIRROR $RELEASE $dist $SRCLIST "$SOURCES"
    if [ -n "$UPDATES" ]; then
        add_entry $MIRROR $RELEASE-updates $dist $SRCLIST "$SOURCES"
    fi
    if [ -n "$SECURITY" ]; then
        add_entry $MIRROR $RELEASE-security $dist $SRCLIST "$SOURCES"
    fi
done

# if a repository that contains a auth string given to debbootstrap or this script
# line in $SRCLIST will contain login name and password which apt will complain.
# Parse it out from $SRCLIST and place it to $AUTHLIST
if grep -Eq "$AUTHREGEX" $SRCLIST; then
    SRCLIST_TMP=$(mktemp)
    while IFS= read -r line
    do
        if echo $line | grep -Eq "$AUTHREGEX" ; then
            MIRROR=$(echo $line | cut -d' ' -f2)

            PROTO=$( echo $MIRROR | sed -r  "s/$AUTHREGEX/\1/" )
            LOGIN=$( echo $MIRROR | sed -r  "s/$AUTHREGEX/\2/" )
            PASSWD=$( echo $MIRROR | sed -r  "s/$AUTHREGEX/\3/" )
            MACHINE=$( echo $MIRROR | sed -r  "s/$AUTHREGEX/\4/" )

            # place $SRCLIST line without credentials to a temp file
            SRC_LINE=$( echo $line | sed 's,'"${MIRROR}"','"${PROTO}${MACHINE}"',' )
            if ! grep -q "$SRC_LINE" ${SRCLIST_TMP}; then
                echo $SRC_LINE >> $SRCLIST_TMP
            fi
            [ -f $AUTHLIST ] || touch $AUTHLIST
            if ! grep -q "^machine .*${PROTO}${MACHINE}" $AUTHLIST; then
                echo "machine ${PROTO}${MACHINE} login ${LOGIN} password ${PASSWD}" >> $AUTHLIST
            fi
            echo $MIRROR
        else
            echo $line >> $SRCLIST_TMP
        fi
    done < "$SRCLIST"

    mv $SRCLIST_TMP $SRCLIST

    if [ -f $AUTHLIST ]; then
        chown root:root $AUTHLIST
        chmod 600 $AUTHLIST
    fi
fi

# the -security and -updates repositories may ship updates to already-installed
# packages, so make sure they get pulled in
export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get upgrade -y --no-install-recommends
