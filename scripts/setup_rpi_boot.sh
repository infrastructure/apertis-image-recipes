#!/bin/bash

set -e

if [ -z  "${ROOTDIR}" ] ; then
  echo "ROOTDIR not given"
  exit 1
fi

osname=$1

bootconf=${ROOTDIR}/boot/loader/entries/ostree-1-${osname}.conf

if [ ! -f "$bootconf" ]; then
    echo "OStree setup is not available!"
    exit 1
fi

ostree=$(grep -o 'ostree=[/.[:alnum:]]\+' $bootconf)
ostree="${ROOTDIR}${ostree#*=}"

mkdir -p ${ROOTDIR}/boot/firmware
cp -v ${ostree}/usr/lib/raspi-firmware/* ${ROOTDIR}/boot/firmware/

# Copy U-Boot
cp -v ${ostree}/usr/lib/u-boot/rpi_arm64/u-boot.bin ${ROOTDIR}/boot/firmware/

# Copy DTBs from u-boot
cp -v ${ostree}/usr/lib/u-boot/rpi_arm64/*.dtb ${ROOTDIR}/boot/firmware/

# Copy DTB Overlays from u-boot
mkdir -p ${ROOTDIR}/boot/firmware/overlays
cp -v ${ostree}/usr/lib/u-boot/rpi_arm64/bcm2711-vl805.dtbo ${ROOTDIR}/boot/firmware/overlays/vl805.dtbo
