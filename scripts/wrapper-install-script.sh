#!/bin/sh

ROOTDIR=$1

if [ "${ROOTDIR}" != "" ]; then
	echo "Using ROOTDIR=\"${ROOTDIR}\""
fi

export SCRIPT=${ROOTDIR}boot.cmd

export ITS=${ROOTDIR}boot.its

cat > ${ITS} << __ITS_EOF
/dts-v1/;

/ {
    description = "U-Boot installation script";
    #address-cells = <1>;

    images {
	default = "script";

        script {
	    data = /incbin/("$SCRIPT");
            type = "script";
            compression = "none";
        };
    };
};
__ITS_EOF

mkimage -f ${ITS} -E ${ROOTDIR}boot.scr
